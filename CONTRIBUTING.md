# Contributing

On Linux, this project requires you to have `ayatana-appindicator3-0.1` or `appindicator3-0.1` installed on your system for the tray icon. See https://pub.dev/packages/tray_manager#quick-start.

*Eventually, the Flutter side should probably just be an interface for some sort of configuration, and a separate service can actually execute commands periodically.*
