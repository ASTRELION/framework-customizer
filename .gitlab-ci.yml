stages:
  - test
  - build
  - deploy

cache:
  when: on_success
  paths:
    - build
    - /usr/bin
    - AppDir

test:
  stage: test
  image: ubuntu:jammy
  before_script:
    - apt update && apt upgrade -y
    - apt install -y curl wget git unzip xz-utils tar zip libglu1-mesa
    - apt install -y clang cmake git ninja-build pkg-config libgtk-3-dev liblzma-dev libstdc++-12-dev
    - apt install -y libayatana-appindicator3-dev
    - wget https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_3.19.3-stable.tar.xz
    - mkdir -p /usr/bin
    - tar -xf flutter_linux_3.19.3-stable.tar.xz -C /usr/bin
    - export PATH="/usr/bin/flutter/bin:$PATH"
    - git config --global --add safe.directory /usr/bin/flutter
    - flutter config --no-cli-animations
    - flutter analyze || true
    - flutter doctor
  script:
    - flutter test

build-linux-bin:
  stage: build
  image: ubuntu:jammy
  before_script:
    - apt update && apt upgrade -y
    - apt install -y curl wget git unzip xz-utils tar zip libglu1-mesa
    - apt install -y clang cmake git ninja-build pkg-config libgtk-3-dev liblzma-dev libstdc++-12-dev
    - apt install -y libayatana-appindicator3-dev
    - wget https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_3.19.3-stable.tar.xz
    - mkdir -p /usr/bin
    - tar -xf flutter_linux_3.19.3-stable.tar.xz -C /usr/bin
    - export PATH="/usr/bin/flutter/bin:$PATH"
    - git config --global --add safe.directory /usr/bin/flutter
    - flutter config --no-cli-animations
    - flutter doctor
  script:
    - flutter build linux --release
  artifacts:
    paths:
      - build/linux/x64/release/bundle

build-appimage:
  stage: build
  image: ubuntu:jammy
  # https://appimage-builder.readthedocs.io/en/latest/intro/install.html#debian-ubuntu
  script:
    - apt update && apt upgrade -y
    - apt install -y wget binutils coreutils desktop-file-utils fakeroot fuse libgdk-pixbuf2.0-dev libgtk-3-dev patchelf python3-pip python3-setuptools squashfs-tools strace util-linux zsync
    - wget -O appimage-builder-x86_64.AppImage https://github.com/AppImageCrafters/appimage-builder/releases/download/v1.1.0/appimage-builder-1.1.0-x86_64.AppImage
    - chmod +x appimage-builder-x86_64.AppImage
    - mv appimage-builder-x86_64.AppImage /usr/bin/appimage-builder
    - appimage-builder --recipe AppImageBuilder.yml --skip-tests
  needs:
    - build-linux-bin
  artifacts:
    paths:
      - "*.AppImage*"

build-windows-bin:
  stage: build
  image: mcr.microsoft.com/windows:ltsc2019
  tags:
    - windows
  allow_failure: true
  before_script:
    - Invoke-WebRequest https://storage.googleapis.com/flutter_infra_release/releases/stable/windows/flutter_windows_3.19.4-stable.zip -OutFile flutter_windows.zip
    - Expand-Archive -Path flutter_windows.zip -Destination $env:USERPROFILE\dev\
    - $currentPath = [Environment]::GetEnvironmentVariable("PATH", "Machine")
    - $newPath = $currentPath + ";C:\$env:USERPROFILE\dev\flutter"
    - '[Environment]::SetEnvironmentVariable("PATH", $newPath, "Machine")'
    - flutter config --no-cli-animations
    - flutter doctor
  script:
    - flutter build windows --release
  artifacts:
    paths:
      - build/windows/x64/release/bundle

# It seems to be virtually impossible (or extremely complicated)
# to build a snap in GitLab CI
# build-snap:
#   stage: build
#   image: ubuntu:latest
#   script:
#     - apt update && apt upgrade -y
#     - apt install snapd -y
#     - groupadd --force --system lxd
#     - usermod -a -G lxd $(whoami)
#     - snap install lxd
#     - lxd init --auto
#     - snap install snapcraft
#     - snapcraft -v
#   artifacts:
#     paths:
#       - "*.snap"
