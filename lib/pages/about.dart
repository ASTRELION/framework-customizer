import "package:flutter/material.dart";
import "package:flutter_gen/gen_l10n/app_localizations.dart";

class AboutPage extends StatelessWidget {
  const AboutPage({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final lang = AppLocalizations.of(context)!;

    return CustomScrollView(
      shrinkWrap: true,
      slivers: [
        SliverList.list(
          children: [
            Text(
              lang.aboutTitle,
              style: Theme.of(context).textTheme.displayMedium,
            ),
            const SizedBox(
              height: 16.0,
            ),
            const Text("This is an about page :)"),
          ],
        ),
      ],
    );
  }
}
