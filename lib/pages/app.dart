import "package:flutter/material.dart";
import "package:flutter/services.dart";
import "package:flutter_localizations/flutter_localizations.dart";
import "package:framework_customizer/pages/about.dart";
import "package:framework_customizer/pages/home.dart";
import "package:framework_customizer/pages/settings.dart";
import "package:tray_manager/tray_manager.dart";
import "package:window_manager/window_manager.dart";
import "package:flutter_gen/gen_l10n/app_localizations.dart";
import "package:adaptive_theme/adaptive_theme.dart";

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with TrayListener, WindowListener {
  @override
  void initState() {
    trayManager.addListener(this);
    windowManager.addListener(this);
    _createTray();
    _init();
    super.initState();
  }

  @override
  void dispose() {
    trayManager.removeListener(this);
    windowManager.removeListener(this);
    super.dispose();
  }

  void _init() async {
    await windowManager.setPreventClose(true);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return AdaptiveTheme(
      light: ThemeData(
        brightness: Brightness.light,
        useMaterial3: true,
        colorSchemeSeed: Colors.blue,
      ),
      dark: ThemeData(
        brightness: Brightness.dark,
        useMaterial3: true,
        colorSchemeSeed: Colors.blue,
      ),
      initial: AdaptiveThemeMode.dark,
      builder: (theme, darkTheme) => MaterialApp(
        onGenerateTitle: (context) => AppLocalizations.of(context)!.title,
        theme: theme,
        darkTheme: darkTheme,
        home: const AppLayout(),
        localizationsDelegates: const [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
      ),
    );
  }

  @override
  void onWindowClose() async {
    windowManager.hide();
    _createTray();
  }

  Future<void> _createTray() async {
    Menu menu = Menu(
      items: [
        MenuItem(
          key: "show_window",
          label:
              await windowManager.isVisible() ? "Hide Window" : "Show Window",
          onClick: (_) async {
            await windowManager.isVisible()
                ? windowManager.hide()
                : windowManager.show();
            _createTray();
          },
        ),
        MenuItem.separator(),
        MenuItem(
          key: "quit",
          label: "Quit",
          onClick: (_) {
            SystemChannels.platform.invokeMethod("SystemNavigator.pop");
          },
        ),
      ],
    );
    await trayManager.setTitle("Framework Customizer");
    await trayManager.setIcon("images/customizer.png");
    await trayManager.setContextMenu(menu);
  }
}

class AppLayout extends StatefulWidget {
  const AppLayout({super.key});

  @override
  State<AppLayout> createState() => _AppLayoutState();
}

class _AppLayoutState extends State<AppLayout> {
  var _selectedIndex = 0;
  var _extended = false;

  @override
  Widget build(BuildContext context) {
    final lang = AppLocalizations.of(context)!;

    Widget page;
    switch (_selectedIndex) {
      case 0:
        page = const HomePage();
        break;
      case 1:
        page = const SettingsPage();
        break;
      case 2:
        page = const AboutPage();
        break;
      default:
        throw UnimplementedError("Page $_selectedIndex not implemented");
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(lang.title),
        backgroundColor: Colors.black26,
      ),
      body: Row(
        children: [
          SafeArea(
            child: MouseRegion(
              onEnter: (_) => setState(() {
                _extended = true;
              }),
              onExit: (_) => setState(() {
                _extended = false;
              }),
              child: NavigationRail(
                extended: _extended,
                backgroundColor: Colors.black12,
                destinations: const [
                  NavigationRailDestination(
                    icon: Icon(Icons.home),
                    label: Text("Home"),
                  ),
                  NavigationRailDestination(
                    icon: Icon(Icons.settings),
                    label: Text("Settings"),
                  ),
                  NavigationRailDestination(
                    icon: Icon(Icons.info),
                    label: Text("About"),
                  ),
                ],
                selectedIndex: _selectedIndex,
                onDestinationSelected: (selected) {
                  setState(() {
                    _selectedIndex = selected;
                  });
                },
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  Expanded(child: page),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
