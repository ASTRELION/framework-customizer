import "package:flutter/material.dart";
import "package:flutter_layout_grid/flutter_layout_grid.dart";
import "package:framework_customizer/framework/framework.dart";
import "package:provider/provider.dart";

class HomePage extends StatelessWidget {
  const HomePage({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final frame = context.watch<FrameworkModel>();

    List<Widget> deviceWidgets = [];
    for (final device in frame.devices) {
      deviceWidgets.add(
        ChangeNotifierProvider(
          create: (_) => device,
          child: const MatrixWidget(),
        ),
      );
    }

    return CustomScrollView(
      shrinkWrap: true,
      slivers: [
        SliverList.list(
          children: [
            Center(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ...deviceWidgets,
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class MatrixWidget extends StatelessWidget {
  const MatrixWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final module = context.watch<MatrixModel>();
    final version = module.getVersion();

    final List<Widget> cells = [];
    for (var r = 0; r < module.grid.length; r++) {
      for (var c = 0; c < module.grid[r].length; c++) {
        cells.add(
          MatrixCell(
            model: module,
            lit: module.grid[r][c],
            x: c,
            y: r,
          ),
        );
      }
    }

    return Expanded(
      child: Row(
        children: [
          Column(
            children: [
              LayoutGrid(
                columnSizes: List.filled(9, 18.px),
                rowSizes: List.filled(34, 18.px),
                gridFit: GridFit.loose,
                children: cells,
              ),
              const SizedBox(
                height: 8.0,
              ),
              OutlinedButton(
                onPressed: () {
                  module.clear();
                },
                child: const Text("Clear"),
              ),
              const SizedBox(
                height: 8.0,
              ),
              Text(
                "Firmware version: $version",
                style: Theme.of(context).textTheme.labelSmall,
              ),
            ],
          ),
          const SizedBox(
            width: 16,
          ),
          Expanded(
            child: BrightnessSlider(
              model: module,
            ),
          ),
        ],
      ),
    );
  }
}

class MatrixCell extends StatefulWidget {
  const MatrixCell({
    super.key,
    required this.model,
    required this.lit,
    required this.x,
    required this.y,
  });

  final MatrixModel model;
  final bool lit;
  final int x;
  final int y;

  @override
  State<MatrixCell> createState() => _MatrixCellState();
}

class _MatrixCellState extends State<MatrixCell> {
  Color _color = Colors.black;
  final Color _baseColor = Colors.white;

  @override
  Widget build(BuildContext context) {
    if (widget.lit) {
      _color = Colors.white;
    } else {
      _color = Colors.black;
    }

    return GestureDetector(
      onTap: () {
        widget.model.setCell(widget.x, widget.y, !widget.lit);
      },
      child: MouseRegion(
        onEnter: (_) => setState(() {
          setState(() {
            _color = Colors.white;
          });
        }),
        onExit: (_) => setState(() {
          setState(() {
            _color = _baseColor;
          });
        }),
        child: Padding(
          padding: const EdgeInsets.all(2.0),
          child: Container(
            color: _color,
          ),
        ),
      ),
    );
  }
}

class BrightnessSlider extends StatelessWidget {
  const BrightnessSlider({
    super.key,
    required this.model,
  });

  final MatrixModel model;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Brightness"),
          ],
        ),
        Expanded(
          child: Slider(
            value: model.brightness.toDouble(),
            min: 0,
            max: 100,
            divisions: 100,
            label: "${model.brightness}",
            onChanged: (value) {
              int brightness = value.toInt();
              if (brightness != model.brightness) {
                model.setBrightness(brightness);
              }
            },
          ),
        ),
        IconButton(
          onPressed: () {},
          icon: const Icon(Icons.link_off),
        ),
      ],
    );
  }
}
