import "package:flutter/material.dart";
import "package:flutter/services.dart";
import "package:flutter_layout_grid/flutter_layout_grid.dart";
import "package:framework_customizer/framework/framework.dart";
import "package:provider/provider.dart";
import "package:flutter_gen/gen_l10n/app_localizations.dart";
import "package:adaptive_theme/adaptive_theme.dart";

class SettingsPage extends StatelessWidget {
  const SettingsPage({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final lang = AppLocalizations.of(context)!;

    final behaviorSettings = [
      ...SyncDelaySetting(context).build(),
    ];

    final appSettings = [
      ...ThemeSetting(context).build(),
    ];

    return CustomScrollView(
      shrinkWrap: true,
      slivers: [
        SliverList.list(
          children: [
            Text(
              lang.settingsTitle,
              style: Theme.of(context).textTheme.displayMedium,
            ),
            const SizedBox(
              height: 16.0,
            ),
          ],
        ),
        SliverList.list(
          children: [
            LayoutGrid(
              columnSizes: [1.fr, 1.fr],
              rowSizes: List.filled(behaviorSettings.length, auto),
              children: behaviorSettings,
            ),
            const Padding(
              padding: EdgeInsets.all(32.0),
              child: Divider(
                thickness: 0.5,
              ),
            ),
            LayoutGrid(
              columnSizes: [1.fr, 1.fr],
              rowSizes: List.filled(appSettings.length, auto),
              children: appSettings,
            ),
          ],
        ),
      ],
    );
  }
}

abstract class Setting {
  Setting(this.context);

  BuildContext context;

  List<Widget> build();
}

class SyncDelaySetting extends Setting {
  SyncDelaySetting(super.context);

  @override
  List<Widget> build() {
    final lang = AppLocalizations.of(context)!;
    final frame = context.watch<FrameworkModel>();
    final controller = TextEditingController(
      text: frame.syncDuration.inSeconds.toString(),
    );

    return [
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            lang.settingsSyncDelayLabel,
            style: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
      TextField(
        controller: controller,
        inputFormatters: [FilteringTextInputFormatter.digitsOnly],
        decoration: const InputDecoration(
          hintText: "55",
        ),
        onChanged: (value) {
          var seconds = int.tryParse(value);
          if (seconds != null && seconds < 5) {
            seconds = 5;
            controller.text = seconds.toString();
          }
          if (seconds != null) {
            frame.setSyncDuration(Duration(seconds: seconds));
          }
        },
      ),
      GridPlacement(
        columnStart: 1,
        child: Column(
          children: [
            const SizedBox(
              height: 16.0,
            ),
            Text(
              lang.settingsSyncDelayDescription,
              style: Theme.of(context).textTheme.labelSmall,
            ),
          ],
        ),
      ),
    ];
  }
}

class ThemeSetting extends Setting {
  ThemeSetting(super.context);

  @override
  List<Widget> build() {
    final lang = AppLocalizations.of(context)!;

    return [
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            lang.settingsThemeLabel,
            style: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
      ThemeDropdown(context: context),
    ];
  }
}

class ThemeDropdown extends StatefulWidget {
  const ThemeDropdown({
    super.key,
    required this.context,
  });

  final BuildContext context;

  @override
  State<ThemeDropdown> createState() => _ThemeDropdownState();
}

class _ThemeDropdownState extends State<ThemeDropdown> {
  int _value = 0;

  @override
  Widget build(BuildContext context) {
    final lang = AppLocalizations.of(context)!;
    final theme = AdaptiveTheme.of(context);

    setState(() {
      switch (theme.mode) {
        case AdaptiveThemeMode.dark:
          _value = 0;
          break;
        case AdaptiveThemeMode.light:
          _value = 1;
          break;
        case AdaptiveThemeMode.system:
          _value = 2;
          break;
      }
    });

    return DropdownButton(
      value: _value,
      items: [
        DropdownMenuItem(
          value: 0,
          child: Text(lang.settingsThemeDark),
        ),
        DropdownMenuItem(
          value: 1,
          child: Text(lang.settingsThemeLight),
        ),
        DropdownMenuItem(
          value: 2,
          child: Text(lang.settingsThemeSystem),
        ),
      ],
      onChanged: (value) {
        final theme = AdaptiveTheme.of(context);
        setState(() {
          _value = value!;
        });
        switch (value) {
          case 0:
            theme.setDark();
            break;
          case 1:
            theme.setLight();
          case 2:
            theme.setSystem();
          default:
            throw UnimplementedError();
        }
      },
    );
  }
}
