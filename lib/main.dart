import "package:flutter/material.dart";
import "package:framework_customizer/framework/framework.dart";
import "package:framework_customizer/pages/app.dart";
import "package:provider/provider.dart";
import "package:window_manager/window_manager.dart";

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await windowManager.ensureInitialized();
  var windowOptions = const WindowOptions(
    center: true,
    title: "Framework Customizer",
    size: Size(
      1024 + 256,
      512 + 256 + 32,
    ),
  );
  windowManager.waitUntilReadyToShow(windowOptions);

  runApp(
    ChangeNotifierProvider(
      create: (_) => FrameworkModel(),
      builder: (context, child) {
        return const MyApp();
      },
    ),
  );
}
