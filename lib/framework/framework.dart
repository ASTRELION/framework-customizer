import "dart:async";
import "dart:typed_data";

import "package:flutter/widgets.dart";
import "package:flutter_libserialport/flutter_libserialport.dart";

/// https://github.com/FrameworkComputer/inputmodule-rs/blob/main/commands.md#command-overview
abstract class _CommandType {
  static int brightness = 0x00;
  static int pattern = 0x01;
  static int bootloader = 0x02;
  static int sleep = 0x03;
  static int getsleep = 0x03;
  static int animate = 0x04;
  static int getanimate = 0x04;
  static int panic = 0x05;
  static int drawbw = 0x06;
  static int stagecol = 0x07;
  static int flushcols = 0x08;
  static int settext = 0x09;
  static int startgame = 0x10;
  static int gamectl = 0x11;
  static int gamestatus = 0x12;
  static int setcolor = 0x13;
  static int displayon = 0x14;
  static int invertscreen = 0x15;
  static int setpxcol = 0x16;
  static int flushfb = 0x17;
  static int version = 0x20;
}

enum Module { smLeft, smRight, md, lg }

class _Command {
  _Command(this.type, this.params, this.device);

  int type;
  List<int> params;
  Module device;

  /// https://github.com/FrameworkComputer/inputmodule-rs/blob/main/commands.md
  List<int> execute({bool read = false}) {
    var serial = "/dev/ttyACM0";
    switch (device) {
      case Module.smLeft:
        serial = "/dev/ttyACM1";
      case Module.smRight:
        serial = "/dev/ttyACM0";
      case _:
        throw UnimplementedError();
    }

    var port = SerialPort(serial);
    if (read) {
      port.openReadWrite();
    } else {
      port.openWrite();
    }
    port.write(Uint8List.fromList([0x32, 0xAC, type] + params), timeout: 0);

    List<int> response = [];
    if (read) {
      response = port.read(3, timeout: 0);
    }

    port.close();
    return response;
  }
}

class FrameworkModel extends ChangeNotifier {
  FrameworkModel._(this._devices);

  factory FrameworkModel() {
    final model = FrameworkModel._([
      MatrixModel(Module.smLeft),
      MatrixModel(Module.smRight),
    ]);
    model.startSync();
    return model;
  }

  Duration _syncDuration = const Duration(seconds: 55);

  List<MatrixModel> _devices;

  Timer? _timer;

  List<MatrixModel> get devices {
    return _devices;
  }

  Duration get syncDuration {
    return _syncDuration;
  }

  @override
  void dispose() {
    stopSync();
    super.dispose();
  }

  void startSync() {
    stopSync();
    _timer = Timer.periodic(syncDuration, (timer) {
      _sync();
    });
  }

  void stopSync() {
    _timer?.cancel();
  }

  void setSyncDuration(Duration syncDuration) {
    _syncDuration = syncDuration;
    stopSync();
    startSync();
    notifyListeners();
  }

  void _sync() {
    for (final device in devices) {
      device.sync();
    }
  }
}

class MatrixModel extends ChangeNotifier {
  MatrixModel(this._module);

  final Module _module;

  int _brightness = 2;
  List<List<bool>> grid = List.generate(34, (_) => List.filled(9, false));

  int get brightness {
    return _brightness;
  }

  int setBrightness(int b) {
    _brightness = b.clamp(0, 100);
    notifyListeners();
    sync();
    return _brightness;
  }

  int getVersion() {
    final response =
        _Command(_CommandType.version, [], _module).execute(read: true);
    final version = (response[0] << 8) + response[1];
    return version;
  }

  void clear() {
    grid = List.generate(34, (_) => List.filled(9, false));
    notifyListeners();
    sync();
  }

  bool setCell(int x, int y, bool lit) {
    grid[y][x] = lit;
    notifyListeners();
    sync();
    return grid[y][x];
  }

  Future<void> sync() async {
    print("Syncing $_module");
    final flattened = grid.expand((element) => element).toList();
    final bytes = List.filled(39, 0x00);
    var b = 0;
    for (var i = 0; i < 39; i++) {
      for (var j = 0; j < 8; j++) {
        if (b < flattened.length && flattened[b]) {
          bytes[i] = bytes[i] | (1 << j % 8);
        }
        b += 1;
      }
    }
    _Command(_CommandType.drawbw, bytes, _module).execute();
    _Command(_CommandType.brightness, [_brightness], _module).execute();
  }
}
