import "dart:ui";

import "package:flutter_test/flutter_test.dart";
import "package:framework_customizer/framework/framework.dart";
import "package:framework_customizer/pages/app.dart";
import "package:provider/provider.dart";

void main() {
  testWidgets("Counter increments smoke test", (WidgetTester tester) async {
    await tester.binding.setSurfaceSize(const Size(1000, 1000));
    await tester.pumpWidget(
      ChangeNotifierProvider(
        create: (_) => FrameworkModel(),
        builder: (context, child) {
          return const MyApp();
        },
      ),
    );
    expect(find.text("Framework Customizer"), findsAny);
  });
}
