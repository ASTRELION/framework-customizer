![Very official logo](images/customizer.png)

# Framework Customizer

[![pipeline status](https://gitlab.com/ASTRELION/framework-customizer/badges/main/pipeline.svg)](https://gitlab.com/ASTRELION/framework-customizer/-/commits/main)

Unofficial customizer for [Framework][framework] laptop input modules ([LED matrix][framework-matrix], [color shift][framework-colorshift], [macropad][framework-macropad], keyboard, etc.) written in [Flutter][flutter].

## Install / Build

### Using an artifact (recommended)

1. Navigate to https://gitlab.com/ASTRELION/framework-customizer/-/artifacts and download the latest artifact generated for your platform (e.g. `build-appimage`)
1. Unzip the resulting archive and install it in the location of your choosing
1. Run the app

### Using AppImage

1. Install [appimage-builder][appimage-install] and [Flutter][flutter-install]
1. Clone and navigate to this repository
    ```bash
    # E.g.
    git clone git@gitlab.com:ASTRELION/framework-customizer.git
    cd framework-customizer
    ```
1. Run `flutter build linux --release`
1. Run `appimage-builder --recipe AppImageBuilder.yml`
1. Run `*.AppImage`

### Using Snap

1. Install [Snapcraft][snapcraft-install]
1. Clone and navigate to this repository
    ```bash
    # E.g.
    git clone git@gitlab.com:ASTRELION/framework-customizer.git
    cd framework-customizer
    ```
1. Run `snapcraft`
1. Install the resulting Snap with `snap install --dangerous --devmode *.snap`
1. Run `snap run framework-customizer`

### Using Flutter Build

1. Install [Flutter][flutter-install]
1. Clone and navigate to this repository
    ```bash
    # E.g.
    git clone git@gitlab.com:ASTRELION/framework-customizer.git
    cd framework-customizer
    ```
1. Build the app
    ```bash
    # E.g.
    flutter build <linux|windows|macos>
    ```
    - If you'd like to just run/try the app, use `flutter run` here instead and skip the remaining steps
1. Copy the resulting bundle in `build/<linux|windows|macos>/x64/release/bundle` to your install location of choice (you need the whole folder, not just the binary)
1. Run the binary
    ```bash
    # E.g.
    build/linux/x64/release/bundle/framework_customizer
    ```

### Update

1. Pull the newest changes from the repository
    ```bash
    # E.g.
    cd framework-customizer
    git pull
    ```
1. Follow the usual remaining install steps

## Contribute

See [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).



[appimage-install]:     https://appimage-builder.readthedocs.io/en/latest/intro/install.html
[flutter]:              https://flutter.dev/
[flutter-install]:      https://docs.flutter.dev/get-started/install
[framework]:            https://frame.work/
[framework-colorshift]: https://frame.work/products/16-spacer?v=FRAKDC0007
[framework-macropad]:   https://frame.work/products/16-rgb-macropad
[framework-matrix]:     https://frame.work/products/16-led-matrix
[snapcraft-install]:    https://snapcraft.io/docs/snapcraft-setup
